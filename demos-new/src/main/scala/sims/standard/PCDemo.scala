/*
 * Copyright (C) 2016-2017, Roberto Casadei, Mirko Viroli, and contributors.
 * See the LICENCE.txt file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package sims.standard

import it.unibo.scafi.incarnations.BasicSimulationIncarnation.{AggregateProgram, Builtins}
import it.unibo.scafi.simulation.gui.incarnation.scafi.bridge.ExportEvaluation.EXPORT_EVALUATION
import it.unibo.scafi.simulation.gui.incarnation.scafi.bridge.ScafiSimulationInitializer.RadiusSimulation
import it.unibo.scafi.simulation.gui.incarnation.scafi.bridge.SimulationInfo
import it.unibo.scafi.simulation.gui.incarnation.scafi.bridge.reflection.Demo
import it.unibo.scafi.simulation.gui.incarnation.scafi.world.ScafiWorldInitializer._
import it.unibo.scafi.simulation.gui.incarnation.scafi.bridge.ScafiWorldIncarnation.EXPORT
import it.unibo.scafi.simulation.gui.incarnation.scafi.configuration.{ScafiProgramBuilder, ScafiWorldInformation}
import it.unibo.scafi.simulation.gui.view.{ViewSetting, WindowConfiguration};
import it.unibo.scafi.space.graphics2D.BasicShape2D.Circle

object PCDemo extends App {

  val formatter_evaluation: EXPORT_EVALUATION[Any] = (e : EXPORT) => formatter(e.root[Any]())

  val formatter: Any => Any = (e) => e match {
    case (a,b) => (formatter(a),formatter(b))
    case (a,b,c) => (formatter(a),formatter(b),formatter(c))
    case (a,b,c,d) => (formatter(a),formatter(b),formatter(c),formatter(d))
    case l:Iterable[_] => l.map(formatter(_)).toString
    case i: java.lang.Number if (i.doubleValue()>10000) => "Inf"
    case i: java.lang.Number if (-i.doubleValue()>10000) => "-Inf"
    case i: java.lang.Double => f"${i.doubleValue()}%1.2f"
    case x => x.toString
  }

  ViewSetting.windowConfiguration = WindowConfiguration(1200,700)
  ViewSetting.labelFontSize = 15

  type ClassToRun = PCMain17

  ScafiProgramBuilder (
    Random(100,1200,700),//Grid(100,12,7,1.0),
    //SimulationInfo(program = classOf[ClassToRun],exportEvaluations = List(formatter_evaluation)),
    SimulationInfo(program = classOf[ClassToRun],exportEvaluations = List(formatter_evaluation)),
    RadiusSimulation(radius = 150),
    scafiWorldInfo = ScafiWorldInformation(
      shape = Some(Circle(3,3))),
    neighbourRender = true
  ).launch()
}

/**
  * there are some demo test, you can change the running programs in SimulationInfo(program = classOf[PCMaini])
  */
abstract class PCDemoAggregateProgram extends AggregateProgram {
  def sense1 = sense[Boolean]("sens1")
  def sense2 = sense[Boolean]("sens2")
  def sense3 = sense[Boolean]("sens3")
  def nbrRange = nbrvar[Double]("nbrRange")
  def senseI1 = if (sense[Boolean]("sens1")) 1 else 0

}

@Demo
class PCMain extends PCDemoAggregateProgram {
  override def main() = 10.1
}

@Demo
class PCMain1 extends PCDemoAggregateProgram {
  override def main() = 1
}

@Demo
class PCMain2 extends PCDemoAggregateProgram {
  override def main() = 2+3
}

@Demo
class PCMain3 extends PCDemoAggregateProgram {
  override def main() = (10,20)
}

@Demo
class PCMain4 extends PCDemoAggregateProgram {
  override def main() = Math.random()
}

@Demo
class PCMain5 extends PCDemoAggregateProgram {
  override def main() = sense1
}

@Demo
class PCMain6 extends PCDemoAggregateProgram {
  override def main() = if (sense1) 10 else 20
}

@Demo
class PCMain7 extends PCDemoAggregateProgram {
  override def main() = mid()
}

@Demo
class PCMain8 extends PCDemoAggregateProgram {
  override def main() = minHoodPlus(nbrRange)
}

@Demo
class PCMain9 extends PCDemoAggregateProgram {
  override def main() = rep(0){_+1}
}

@Demo
class PCMain10 extends PCDemoAggregateProgram {
  override def main() = rep(Math.random()){x=>x}
}

@Demo
class PCMain11 extends PCDemoAggregateProgram {
  override def main() = rep[Double](0.0){x => x + rep(Math.random()){y=>y}}
}

@Demo
class PCMain12 extends PCDemoAggregateProgram {
  /*import Builtins.Bounded.of_i
  override def main() = maxHood(boolToInt(nbr{sense1}))*/
  override def main() = foldhood(0)(_ max _)(nbr{senseI1})
}

@Demo
class PCMain13 extends PCDemoAggregateProgram {
  override def main() = foldhoodPlus(0)(_+_){nbr{1}}
}

@Demo
class PCMain14 extends PCDemoAggregateProgram {
  import Builtins.Bounded.of_i

  override def main() = rep(0){ x => senseI1 max maxHoodPlus( nbr{x}) }
}

@Demo
class PCMain15 extends PCDemoAggregateProgram {
  override def main() = rep(Double.MaxValue){ d => mux[Double](sense1){0.0}{minHoodPlus(nbr{d}+1.0)} }
}

@Demo
class PCMain16 extends PCDemoAggregateProgram {
  override def main() = rep(Double.MaxValue){ d => mux[Double](sense1){0.0}{minHoodPlus(nbr{d}+nbrRange)} }
}

@Demo
class PCMain17 extends PCDemoAggregateProgram {
  def gradient(src: Boolean) = rep(Double.MaxValue){ d => mux[Double](src){0.0}{minHoodPlus(nbr{d}+nbrRange)} }
  override def main() = branch(sense2){Double.MaxValue}{gradient(sense1)}
}
